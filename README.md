### NOTE
Project uses strict c99 procedural style to comply with cource's requirements

### Requirements

* C99 compliant compiler like `gcc` or `clang`
* (optional) `Make` to utilize Makefile

Makefile assumes unix environment

Tested compiling and working on 

```
Linux arch 4.20.10-arch1-1-ARCH

gcc version 8.2.1 20181127 (GCC)
GNU Make 4.2.1
```

### Description

This project compares execution speed for various sorting algorithms

Included algorithms : 
* Quick Sort
* Shell Sort

*  Insertion Sort

### Compiling

Makefile assumes unix environment

To compile the executable use 
```
make clean all
```
This will clean the project directory of previous compilations and produce an executable named "main".

Alternative:
Project only contains one source file main.c in src/ directory. You can manually compile it with your favorite C compiler.


### Usage

To run the program simply run "main"
```
./main
```

Program will run sorting tests on 1 CPU using same randomly generated set and will display statistics in stdout.
