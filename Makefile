#makefile for executable named "main"
CFLAGS := $(CFLAGS) -c -Wall -Werror -std=c99 -Og
OBJECTS=main.o
target=main
SRCDIR=src
CC ?= gcc
LD ?= gcc

#Make sure to use DEL for windows as RM
RM ?= /bin/rm -f

all: main
#create the executable
main : $(OBJECTS)
	$(CC) -o $(target) $(OBJECTS)
#create the objects for executable
main.o :
	$(CC) $(CFLAGS) $(SRCDIR)/main.c
#rule to clean all files
.PHONY : clean
clean :
	$(RM) $(target) $(OBJECTS)
