#include <stdio.h>
// printf()
#include <stdlib.h>
// rand() srand() sizeof()
#include <time.h>
//clock(), time()

//Constants
static const unsigned int totalTests = 20;
static const unsigned int maxArraySize = 32 * 1024;
static const unsigned int startingArraySize = 1024;
static const unsigned int multiplier = 2;

/*Reverses an array*/
void ReverseArray(int a[], size_t arraysize) {
    int t;
    for (int i = 0; i < arraysize / 2; ++i) {
        t = a[i];
        a[i] = a[(arraysize - 1) - i];
        a[(arraysize - 1) - i] = t;
    }
}

/* Finds logarithm base 2 of a positive number
 *
 * Used in gap creation for shell sort*/
int BinaryLog(size_t temp) {
    int n = 0;
    while (temp > 1) {
        temp = temp >> 1;
        n++;
    }
    return n;
}
/* Swaps two elements of an array */
void Swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
//Insertion Sort
void InsertionSort(int *array, size_t arraySize, long *comparsions, long *swaps) {
    if (arraySize < 2) { return; }
    for (int i = 1; i < arraySize; i++) {
        *comparsions += 1;
        for (int j = 0; j < i; j++) {
            *comparsions = *comparsions + 1;
            if (array[i] < array[j]) {
                *swaps = *swaps + 1;
                Swap(&array[j], &array[i]);
            }
            *comparsions += 1;
        }
        *comparsions += 1;
    }
    *comparsions += 1;
}
//Shell sort using log2 gaps
void ShellSort(int *array, size_t arraySize, long *comparsions, long *swaps) {
    size_t n = (size_t) BinaryLog(arraySize);
    int *gaps = (int *) malloc(((int) n) * sizeof(int));
    for (int i = 0; i < n; i++) {
        gaps[n - 1 - i] = (2 << i) - 1;
    }
    for (int a = 0; a < n; a++) {
        *comparsions += 1;
        int gap = gaps[a];
        for (int i = gap; i < arraySize; i++) {
            *comparsions += 1;
            int val = array[i];
            int j;
            for (j = i - gap; j >= 0 && array[j] > val; j -= gap) {
                *comparsions = *comparsions + 2;
                *swaps = *swaps + 1;
                array[j + gap] = array[j];
            }
            *comparsions += 1;
            array[j + gap] = val;
            *swaps = *swaps + 1;
        }
    }
    *comparsions += 1;
    free(gaps);
}
//Recursive quick sort implementation
void Quick(int *array, int start, int end, long *comparsions, long *swaps) {
    if (start < end) {
        int l = start + 1, r = end, p = array[start];
        while (l < r) {
            *comparsions = *comparsions + 1;
            if (array[l] <= p) {
                l++;
            } else {
                if (array[r] >= p) {
                    r--;
                } else {
                    *swaps = *swaps + 1;
                    Swap(&array[l], &array[r]);
                }
                *comparsions += 1;

            }
            *comparsions += 1;
        }
        *comparsions = *comparsions + 1;
        if (array[l] < p) {
            *swaps = *swaps + 1;
            Swap(&array[l], &array[start]);
            l--;
        } else {
            l--;
            *swaps = *swaps + 1;
            Swap(&array[l], &array[start]);
        }
        *comparsions += 1;
        Quick(array, start, l, comparsions, swaps);
        Quick(array, r, end, comparsions, swaps);
    }
    *comparsions += 1;
}
//QuickSort(array, array size, comparsions tracker, swaps tracker)
void QuickSort(int *a, size_t n, long *comparsions, long *swaps) {
    Quick(a, 0, (int) (n - 1), comparsions, swaps);
}
/*Writes output to file "output" in working dir.
Prints and runs sorting algorithms on array filled with random numbers several times
Gathers statistics for each run and for each set
Prints statistics to stdout*/
void GatherStatistics() {
    srand((unsigned int) time(NULL));
    FILE *file = fopen("output", "w");
    for (size_t currentsize = startingArraySize; currentsize < maxArraySize; currentsize *= multiplier) {
        printf("\n\n\tTesting for %li elements:\n", (long int) currentsize);
        fprintf(file, "Array size: %li\n", (long int) currentsize);
        size_t arraysize = currentsize;
        int a[totalTests][arraysize];
        int a1[totalTests][arraysize];
        int a2[totalTests][arraysize];
        long totaltime, totalswaps, totalcomparsions;
        //statistics
        totaltime = 0;
        totalswaps = 0;
        totalcomparsions = 0;
        for (int l = 0; l < totalTests; ++l) {
            for (size_t i = 0; i < arraysize; i++) {
                a[l][i] = rand() % 10000;
            }
        }
        for (int m = 0; m < totalTests; ++m) {
            for (int j = 0; j < arraysize; ++j) {
                a1[m][j] = a[m][j];
            }
            for (int k = 0; k < arraysize; ++k) {
                a2[m][k] = a[m][k];
            }
        }
        //method call, statistics gathering
        for (size_t tests = 0; tests < totalTests; tests++) {

            /*  for (size_t i = 0; i < arraysize; i++) {
                  a[i] = rand() % 10000;
              }
  */
            long comparsions = 0;
            long swaps = 0;
            long time = (long) clock();
            QuickSort(a[tests], arraysize, &comparsions, &swaps);
            totaltime += (long) clock() - time;
            totalcomparsions += comparsions;
            totalswaps += swaps;
        }
        //print statistics
        printf("On average after %i totalTests, Quicksort took %li clicks, %lf milliseconds, %li swaps, %li comparsions, %lf μseconds per element\n",
               totalTests, totaltime / totalTests, (double) totaltime / totalTests / CLOCKS_PER_SEC * 1000,
               totalswaps / totalTests,
               totalcomparsions / totalTests, (double) totaltime / totalTests / CLOCKS_PER_SEC * 1000000 / currentsize);
        fprintf(file, "For quicksort last array after sorting is:\n");
        for (int n = 0; n < currentsize; ++n) {
            fprintf(file, "%i ", a[totalTests - 1][n]);
        }
        fprintf(file, "\n");
        //statistics
        totaltime = 0;
        totalswaps = 0;
        totalcomparsions = 0;
        //method call, statistics gathering
        for (size_t tests = 0; tests < totalTests; tests++) {
            long comparsions = 0;
            long swaps = 0;
            long time = (long) clock();
            ShellSort(a1[tests], arraysize, &comparsions, &swaps);
            totaltime += (long) clock() - time;
            totalcomparsions += comparsions;
            totalswaps += swaps;
        }
        //print statistics
        printf("On average after %i totalTests, ShellSort took %li clicks, %lf milliseconds, %li swaps, %li comparsions, %lf μseconds per element\n",
               totalTests, totaltime / totalTests, (double) totaltime / totalTests / CLOCKS_PER_SEC * 1000,
               totalswaps / totalTests,
               totalcomparsions / totalTests, (double) totaltime / totalTests / CLOCKS_PER_SEC * 1000000 / currentsize);
        fprintf(file, "For ShellSort last array after sorting is:\n");
        for (int n = 0; n < currentsize; ++n) {
            fprintf(file, "%i ", a[totalTests - 1][n]);
        }
        fprintf(file, "\n");
        //statistics
        totaltime = 0;
        totalswaps = 0;
        totalcomparsions = 0;
        //method call, statistics gathering
        for (size_t tests = 0; tests < totalTests; tests++) {
            long comparsions = 0;
            long swaps = 0;
            long time = (long) clock();
            InsertionSort(a2[tests], arraysize, &comparsions, &swaps);
            totaltime += (long) clock() - time;
            totalcomparsions += comparsions;
            totalswaps += swaps;
        }
        //print statistics
        printf("On average after %i totalTests, InsertionSort took %li clicks, %lf milliseconds, %li swaps, %li comparsions, %lf μseconds per element\n",
               totalTests, totaltime / totalTests, (double) totaltime / totalTests / CLOCKS_PER_SEC * 1000,
               totalswaps / totalTests,
               totalcomparsions / totalTests, (double) totaltime / totalTests / CLOCKS_PER_SEC * 1000000 / currentsize);
        fprintf(file, "For quicksort last array after sorting is:\n");
        for (int n = 0; n < currentsize; ++n) {
            fprintf(file, "%i ", a[totalTests - 1][n]);
        }
        fprintf(file, "\n\n");
    }


}

int main() {
    GatherStatistics();
    return 0;
}
